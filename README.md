# SocketServer

> Wrapper around Socket.io

## Usage

```javascript
const SocketServer = require('@krknet/socket-server')
const socketServer = new SocketServer(httpServer.rawServer, { initiatiors: [] })
global.__sockets = socketServer

await socketServer.start()
console.log(socketServer.isRunning)
await socketServer.stop()
```

Client Path: node_modules/socket.io/client-dist/socket.io.js
Production Client Path: node_modules/socket.io/client-dist/socket.io.min.js

## Options

```javascript
{
  initiatiors: [],
  doLog: true,
  path: '/ws'
}
```

## Functions

```javascript
async start ()

emit (...params)

of (ns)

in (room)

to (recipient)

async stop ()
```
