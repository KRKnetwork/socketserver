const Server = require('socket.io')
const Profiler = require('@krknet/profiler')

module.exports = class SocketServer {
  constructor (rawServer, options = {}) {
    this.options = options
    this.isRunning = false

    this.options.path = this.options.path || '/ws'
    if (this.options.doLog === undefined) this.options.doLog = true
    if (this.options.serveClient === undefined) this.options.serveClient = false
    this.options.initiatiors = this.options.initiatiors || []

    this.rawServer = rawServer
    this.io = Server({
      path: this.options.path,
      serveClient: this.options.serveClient,
      cors: this.options.cors || undefined
    })
  }

  async start () {
    this.io.attach(this.rawServer)

    if (Array.isArray(this.options.initiatiors)) for (const initiatior of this.options.initiatiors) await initiatior(this)

    this.isRunning = true
    if (this.options.doLog) Profiler.success(`Socket-Server started - ${this.options.path}`)
    if (this.hasEventbus) global.__eventBus.emit('system', 'socket', 'up')
  }

  emit (...params) { this.io.emit(...params) }
  of (ns) { return this.io.of(ns) }
  in (room) { return this.io.in(room) }
  to (recipient) { return this.io.to(recipient) }

  async stop (timeout = 1000) {
    const profiler = this.options.doLog ? new Profiler('Socket-Server stopping').start() : null
    const forcer = setTimeout(() => this.forceStop(), timeout)

    return new Promise((resolve, reject) => {
      if (!this.isRunning) return resolve()
      this.io.close(() => {
        this.isRunning = false
        if (this.hasEventbus) global.__eventBus.emit('system', 'socket', 'down')
        resolve()
      })
    })
      .then(() => {
        clearTimeout(forcer)
        if (profiler) profiler.succeed()
      })
  }

  async forceStop () {
    return this.io.disconnectSockets()
  }

  get hasEventbus () { return global && global.__eventBus && global.__eventBus.emit }
}
